public class MineSweeper{
public static void recShowMines(char[][] board, int row, int col) {


		if(!onBoard(board,row,col))
			return;

		if (board[row][col] != '-' && !isMine(board, row, col)) 
			return; 

		if(isMine(board,row,col)) {
			return;	
		}
		else if(!isMine(board,row,col)) {

			if(numAdjacentMines(board,row,col) != 0) {
				
				board[row][col] = (char)(numAdjacentMines(board, row, col) + '0');
				recShowMines(board,row,col-1);
				recShowMines(board,row,col+1);
				recShowMines(board,row+1,col);
				recShowMines(board,row-1,col);
			}

		}

	}

	public static void showMines(char[][] board, int row, int col) {
		if(isMine(board,row,col)) {
			replaceMines(board);
		}
		else {
			recShowMines(board, row, col);
		}
	}
	private static void  displayMines(char[][] board) {

		for(int i=0;i<board.length;i++) {
			for(int j=0;j<board[i].length;j++) {

				System.out.print(board[i][j]);
			}
			System.out.println();
		}


	}
	private static void replaceMines(char [][] board) {
		int count;
		for(int i=0;i<board.length;++i) {
			for(int j=0;j<board[i].length;++j) {
				if(!isMine(board, i, j)) {
					count = numAdjacentMines(board,i,j);
					board[i][j] = (char) (count + '0');
				}

			}
		}
	}
	public static boolean onBoard(char [][] board, int row, int col) {
		return row >= 0  && row < board.length && col >=0 && col < board[row].length;

	}
	private static boolean isMine(char[][] board, int row, int col) {
		return onBoard(board, row, col) && board[row][col] == '*';
	}

	private static int numAdjacentMines(char[][] board, int row, int col) {

		int count = 0;

		for (int i = -1; i < 2; i++)
			for (int j = -1; j < 2; j++)
				if (!(i == 0 && j == 0) && isMine(board, row-i, col-j))
					count++;

		return count;
	}

	public static void main(String[] args) {
		char[][] actual = { 
				{ '*', '-', '-', '-', '-' }, 
				{ '*', '-', '*', '*', '-' }, 
				{ '*', '-', '-', '*', '-' },
				{ '*', '*', '*', '*', '-' }, 
				{ '-', '-', '-', '-', '*' }, 
		};

		showMines(actual,0,1);


	}
}
